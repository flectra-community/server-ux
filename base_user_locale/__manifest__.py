# Copyright 2019 Brainbean Apps (https://brainbeanapps.com)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

{
    "name": "User Locale Settings",
    "version": "2.0.1.1.0",
    "author": "Brainbean Apps, Odoo Community Association (OCA)",
    "category": "Usability",
    "license": "AGPL-3",
    "depends": ["base_setup", "calendar"],
    "website": "https://gitlab.com/flectra-community/server-ux",
    "data": ["views/res_config_settings.xml", "views/res_users.xml"],
    "installable": True,
}
