# Flectra Community / server-ux

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[base_tier_validation_report](base_tier_validation_report/) | 2.0.1.0.0| Reports related to tier validation
[base_cancel_confirm](base_cancel_confirm/) | 2.0.1.0.3| Base Cancel Confirm
[base_substate](base_substate/) | 2.0.1.0.0| Base Sub State
[base_search_custom_field_filter](base_search_custom_field_filter/) | 2.0.1.1.0| Add custom filters for fields via UI
[filter_multi_user](filter_multi_user/) | 2.0.1.0.0| Allows to share user-defined filters filters among several users.
[base_import_security_group](base_import_security_group/) | 2.0.1.0.0| Group-based permissions for importing CSV files
[sequence_reset_period](sequence_reset_period/) | 2.0.1.0.0| Auto-generate yearly/monthly/weekly/daily sequence period ranges
[base_menu_visibility_restriction](base_menu_visibility_restriction/) | 2.0.1.0.0| Restrict (with groups) menu visibilty
[multi_step_wizard](multi_step_wizard/) | 2.0.1.0.0| Multi-Steps Wizards
[mass_operation_abstract](mass_operation_abstract/) | 2.0.1.0.2| Abstract Tools used for modules that realize operation on many items
[base_revision](base_revision/) | 2.0.1.1.0| Keep track of revised document
[document_quick_access](document_quick_access/) | 2.0.1.0.0|         Document quick access
[base_tier_validation_forward](base_tier_validation_forward/) | 2.0.1.0.2| Forward option for base tiers
[base_technical_features](base_technical_features/) | 2.0.1.1.1| Access to technical features without activating debug mode
[base_tier_validation_formula](base_tier_validation_formula/) | 2.0.2.0.1|         Formulas for Base tier validation
[mass_editing](mass_editing/) | 2.0.1.1.0| Mass Editing
[date_range](date_range/) | 2.0.2.1.1| Manage all kind of date range
[chained_swapper](chained_swapper/) | 2.0.1.0.0| Chained Swapper
[date_range_account](date_range_account/) | 2.0.1.0.0| Add Date Range menu entry in Invoicing app
[base_export_manager](base_export_manager/) | 2.0.1.0.1| Manage model export profiles
[sequence_range_end](sequence_range_end/) | 2.0.1.0.0| Sequence prefix/suffix option, 'range_end_', to use the beginning of the range
[base_tier_validation_server_action](base_tier_validation_server_action/) | 2.0.1.1.2| Add option to call server action when a tier is validated
[base_custom_filter](base_custom_filter/) | 2.0.1.0.0| Add custom filters in standard filters and group by dropdowns
[sequence_check_digit](sequence_check_digit/) | 2.0.1.0.2| Adds a check digit on sequences
[server_action_domain](server_action_domain/) | 2.0.1.0.0| Apply a domain filter before executing server actions on records
[document_quick_access_folder_auto_classification](document_quick_access_folder_auto_classification/) | 2.0.2.0.0|         Auto classification of Documents after reading a QR
[base_tier_validation](base_tier_validation/) | 2.0.2.10.0| Implement a validation process based on tiers.
[default_multi_user](default_multi_user/) | 2.0.1.0.0| Allows to share user-defined defaults among several users.
[base_user_locale](base_user_locale/) | 2.0.1.1.0| User Locale Settings
[base_optional_quick_create](base_optional_quick_create/) | 2.0.1.0.0| Avoid "quick create" on m2o fields, on a "by model" basis
[barcode_action](barcode_action/) | 2.0.1.0.1| Allows to use barcodes as a launcher


