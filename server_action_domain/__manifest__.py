# Copyright 2020 Iván Todorovich (https://twitter.com/ivantodorovich)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
{
    "name": "Server Action Domain",
    "version": "2.0.1.0.0",
    "summary": "Apply a domain filter before executing server actions on records",
    "author": "Iván Todorovich, Odoo Community Association (OCA)",
    "category": "Tools",
    "website": "https://gitlab.com/flectra-community/server-ux",
    "license": "AGPL-3",
    "depends": ["base"],
    "data": [
        "views/ir_actions_server.xml",
    ],
}
